import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } 
from '@angular/http';
import { Observable } from 'rxjs/Observable';
// Observable class extensions
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

import { Item } from './item';

@Injectable()
export class ItemService {
 headers: Headers;
 options: RequestOptions;
 private getURL = 'http://localhost:3000/api/get';
 private updateURL = 'http://localhost:3000/api/update';  // URL to web api
 private deleteURL = 'http://localhost:3000/api/delete';
 private createURL = 'http://localhost:3000/api/create';
  constructor(private http: Http) {
    this.headers = new Headers({ 'Content-Type': 'application/json', 
    'Accept': 'q=0.8;application/json;q=0.9' });
this.options = new RequestOptions({ headers: this.headers });
   }
  //get items from server
  getItems() : Observable<Item[]> {
    return this.http
    .get(this.getURL)
    .map(res => res.json())
    .catch(this.handleError);

  }
  //insert items from server
  updateItem (item: Item[]): Observable<Item[]> {
    let body = JSON.stringify(item);
    return this.http
    .post(this.updateURL, body, this.options)
    .map(this.extractData)
    .catch(this.handleError);
   
  }
  //delete item from server
  deleteItem (id) {
    let body = JSON.stringify(id);
    return this.http
    .post(this.deleteURL, body, this.options)
    .map(this.extractData)
    .catch(this.handleError);
   
  }
  //create item from server
  createItem (): Observable<Item[]> {
    return this.http
    .get(this.getURL)
    .map(res => res.json())
    .catch(this.handleError);
   
  }
  private extractData(res: Response) {
    let body = res.json();
    return body || {};
  }
  //handle error
  private handleError(error: any) {
    let errMsg = (error.message) ? error.message :
        error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}