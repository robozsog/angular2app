import { Component, OnInit } from '@angular/core';
import { Item } from './item';
import { ItemService } from './item.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  /*
  test
  items = [
    new Item(1,""),
    new Item(2,""),
    new Item(3,""),
    new Item(4,"")
  ];
  */
  items: Item[];
  constructor(private itemService: ItemService) { }
  ngOnInit() {
    this.getItems();
  }
  getItems(): void {
    this.itemService.getItems()
    .subscribe(items => this.items = items);
  }
  updateItem(item) {
    this.itemService.updateItem(item).subscribe();
  }
  deleteItem(id) {
    this.itemService.deleteItem(id).subscribe();
  }
  createItem() {
    this.itemService.createItem().subscribe();
  }
  
}

